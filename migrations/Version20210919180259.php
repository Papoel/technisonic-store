<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210919180259 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Création de la base de donnée - ajout de données d'entrée";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie (id INT AUTO_INCREMENT NOT NULL, image_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_497DD6343DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE chantier (id INT AUTO_INCREMENT NOT NULL, site_id INT DEFAULT NULL, responsable_mission_id INT DEFAULT NULL, tranche INT NOT NULL, code_arret VARCHAR(255) DEFAULT NULL, otp VARCHAR(50) DEFAULT NULL, INDEX IDX_636F27F6F6BD1646 (site_id), INDEX IDX_636F27F64A455D90 (responsable_mission_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ext_log_entries (id INT AUTO_INCREMENT NOT NULL, action VARCHAR(8) NOT NULL, logged_at DATETIME NOT NULL, object_id VARCHAR(64) DEFAULT NULL, object_class VARCHAR(191) NOT NULL, version INT NOT NULL, data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', username VARCHAR(191) DEFAULT NULL, INDEX log_class_lookup_idx (object_class), INDEX log_date_lookup_idx (logged_at), INDEX log_user_lookup_idx (username), INDEX log_version_lookup_idx (object_id, object_class, version), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB ROW_FORMAT = DYNAMIC');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materiels (id INT AUTO_INCREMENT NOT NULL, utilisateur_id INT DEFAULT NULL, categorie_id INT DEFAULT NULL, affectation_id INT DEFAULT NULL, technique_id INT DEFAULT NULL, reference VARCHAR(255) NOT NULL, marque VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, dimension VARCHAR(255) DEFAULT NULL, frequence VARCHAR(20) DEFAULT NULL, observation TINYTEXT DEFAULT NULL, calibrated_at DATETIME DEFAULT NULL, end_calibrated_at DATETIME DEFAULT NULL, quantity INT NOT NULL, is_avalaible TINYINT(1) NOT NULL, is_in_verification TINYINT(1) NOT NULL, pv_name VARCHAR(255) DEFAULT NULL, updated_at DATETIME NOT NULL, INDEX IDX_9C1EBE69FB88E14F (utilisateur_id), INDEX IDX_9C1EBE69BCF5E72D (categorie_id), INDEX IDX_9C1EBE696D0ABA22 (affectation_id), INDEX IDX_9C1EBE691F8ACB26 (technique_id), FULLTEXT INDEX IDX_9C1EBE695A6F91CE8CDE5729 (marque, type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, adress VARCHAR(255) DEFAULT NULL, code_postal INT DEFAULT NULL, town VARCHAR(255) DEFAULT NULL, telephone INT DEFAULT NULL, departement INT DEFAULT NULL, palier VARCHAR(255) DEFAULT NULL, powerfull INT DEFAULT NULL, tranche INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technique (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, is_autorised TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE categorie ADD CONSTRAINT FK_497DD6343DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE chantier ADD CONSTRAINT FK_636F27F6F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE chantier ADD CONSTRAINT FK_636F27F64A455D90 FOREIGN KEY (responsable_mission_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE69FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE69BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE696D0ABA22 FOREIGN KEY (affectation_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE materiels ADD CONSTRAINT FK_9C1EBE691F8ACB26 FOREIGN KEY (technique_id) REFERENCES technique (id)');

        $this->addSql("INSERT INTO user (firstname, lastname, email, password, roles, is_autorised) values('Pascal', 'Briffard', 'admin@technisonic.fr', '\$2y\$13\$peE9KVzDbjq8j0xB2j1Sy.Diu5/qLanH2Hf5nwxXyP210W2stsN52', '[\"ROLE_USER\", \"ROLE_MAGASINIER\", \"ROLE_ADMIN\"]', 1)");
        $this->addSql("INSERT INTO user (firstname, lastname, email, password, roles, is_autorised) values('Honore', 'Hounwanou', 'magasinier@technisonic.fr', '\$2y\$13\$peE9KVzDbjq8j0xB2j1Sy.Diu5/qLanH2Hf5nwxXyP210W2stsN52', '[\"ROLE_MAGASINIER\"]', 1)");
        $this->addSql("INSERT INTO user (firstname, lastname, email, password, roles, is_autorised) values('Romuald', 'Claes', 'user@technisonic.fr', '\$2y\$13\$peE9KVzDbjq8j0xB2j1Sy.Diu5/qLanH2Hf5nwxXyP210W2stsN52', '[\"ROLE_USER\"]', 0)");

        $this->addSql("INSERT INTO site (name) values('belleville')");
        $this->addSql("INSERT INTO site (name) values('blayais')");
        $this->addSql("INSERT INTO site (name) values('bugey')");
        $this->addSql("INSERT INTO site (name) values('cattenom')");
        $this->addSql("INSERT INTO site (name) values('chinon')");
        $this->addSql("INSERT INTO site (name) values('chooz')");
        $this->addSql("INSERT INTO site (name) values('civaux')");
        $this->addSql("INSERT INTO site (name) values('cruas')");
        $this->addSql("INSERT INTO site (name) values('dampierre')");
        $this->addSql("INSERT INTO site (name) values('dunkerque')");
        $this->addSql("INSERT INTO site (name) values('flamanville')");
        $this->addSql("INSERT INTO site (name) values('gravelines')");
        $this->addSql("INSERT INTO site (name) values('golfech')");
        $this->addSql("INSERT INTO site (name) values('manom')");
        $this->addSql("INSERT INTO site (name) values('nogent')");
        $this->addSql("INSERT INTO site (name) values('paluel')");
        $this->addSql("INSERT INTO site (name) values('penly')");
        $this->addSql("INSERT INTO site (name) values('saint alban')");
        $this->addSql("INSERT INTO site (name) values('saint laurent')");
        $this->addSql("INSERT INTO site (name) values('tricastin')");

        $this->addSql("INSERT INTO categorie (name) values ('poste ultrason')");
        $this->addSql("INSERT INTO categorie (name) values ('petit materiel')");
        $this->addSql("INSERT INTO categorie (name) values ('palpeur')");
        $this->addSql("INSERT INTO categorie (name) values ('produit')");
        $this->addSql("INSERT INTO categorie (name) values ('general')");

        $this->addSql("INSERT INTO technique (name) values ('visuel')");
        $this->addSql("INSERT INTO technique (name) values ('ressuage')");
        $this->addSql("INSERT INTO technique (name) values ('magnetoscopie')");
        $this->addSql("INSERT INTO technique (name) values ('ultrason')");
        $this->addSql("INSERT INTO technique (name) values ('radiographie')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE69BCF5E72D');
        $this->addSql('ALTER TABLE categorie DROP FOREIGN KEY FK_497DD6343DA5256D');
        $this->addSql('ALTER TABLE chantier DROP FOREIGN KEY FK_636F27F6F6BD1646');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE696D0ABA22');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE691F8ACB26');
        $this->addSql('ALTER TABLE chantier DROP FOREIGN KEY FK_636F27F64A455D90');
        $this->addSql('ALTER TABLE materiels DROP FOREIGN KEY FK_9C1EBE69FB88E14F');
        $this->addSql('DROP TABLE categorie');
        $this->addSql('DROP TABLE chantier');
        $this->addSql('DROP TABLE ext_log_entries');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE materiels');
        $this->addSql('DROP TABLE site');
        $this->addSql('DROP TABLE technique');
        $this->addSql('DROP TABLE `user`');
    }
}
