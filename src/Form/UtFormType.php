<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\User;
use App\Entity\Materiel;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class UtFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marque', TextType::class, [
                'attr' => ['autofocus' => true],
                'help' => 'Entrer le marque du matériel'
            ])

            ->add('type', TextType::class, [
                'required'   => true,
                'help' => 'Entrer le type du matériel'
            ])

            ->add('reference', TextType::class, [
                'required'   => true,
                'help' => 'Entrer la référence du matériel'
            ])

            ->add('dimension', TextType::class, [
                'required'   => false,
                'help' => 'Entrer les dimensions ou le Ø'
            ])

            ->add('frequence', NumberType::class,[
                'required'   => false,
                'help' => 'Entrer la fréquence en Mhz'
            ])

            ->add('pvFile', VichFileType::class, [
                'label' => false,
                'required' => false,
                'download_link' => true,
                'attr' => ['accept' => 'application/pdf'],
                'constraints' => [
                    new File(Materiel::CERTFICAT_VALIDATION_CONSTRAINTS),
                ],
            ])

            ->add('affectation', EntityType::class, [
                'class' => Site::class,
                'choice_label' => 'name',
                'label' => 'Colisage du matériel'
            ])

            ->add('utilisateur', EntityType::class, [
                'class' => User::class,
                'required' => 'false',
                'choice_label' => 'FullName',
                'label' => 'Dotation du matériel'
            ])

            ->add('observation', TextareaType::class, [
                'help' => 'Ajouter une notes',
                'required'   => false,
                'label_attr' => [
                    'class' => 'text-danger'
                ],
                'attr' => [
                    'class' => 'text-danger',
                    'placeholder' => 'Inscrivez ici vos observation ou informations',
                    'rows' => 5
                    ]
            ])

            ->add('categorie', EntityType::class, [
                'class' => Categorie::class,
                'required' => true,
                'choice_label' => 'name',
                // 'label_attr' => ['class' => 'fw-bold'],
                'attr' => [
                    'class' => 'text-capitalize',
                ]
            ])

            ->add('calibrated_at', DateType::class, [
                'required' => false,
                'label' => "Date d'étalonnage"
             ])

             ->add('end_calibrated_at', DateType::class, [
                'required' => false,
                'label' => "Fin de validité"
             ])

             ->add('isAvalaible', null, [
                'row_attr' => [
                    'class' => 'form-switch form-check'
                ],
                'label' => "Matériel disponible",
                'required'   => false
            ])

            ->add('isInVerification', null, [
                'row_attr' => [
                    'class' => 'form-switch form-check'
                ],
                'label' => 'Non disponible',
                'required'   => false
            ])

            ->add('quantity', NumberType::class, [
                'attr' => [
                    'min' => 0
                ],
                'html5' => true,
                'label' => 'Quantité',
                'required'   => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Materiel::class
        ]);
    }
}
