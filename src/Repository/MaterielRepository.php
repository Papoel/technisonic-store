<?php

namespace App\Repository;

use App\Entity\Materiel;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Materiel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Materiel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Materiel[]    findAll()
 * @method Materiel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaterielRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Materiel::class);
    }

    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'ASC'));
    }

    /**
     * Rechercher le materiel en fonction du formulaire de rechercehe
     * @return void
     */
    public function search($mots = null, $categorie = null, $affectation = null, $technique = null)
    {
        $query = $this->createQueryBuilder('m');
        if ($mots != null) {
            $query->Where('MATCH_AGAINST(m.marque, m.type) AGAINST(:mots boolean)>0')
                ->setParameter('mots', $mots);
        }

        if ($categorie != null) {
            $query->leftJoin('m.categorie', 'c');
            $query->andWhere('c.id = :id')
                  ->setParameter('id', $categorie);
        }

        if ($affectation != null) {
            $query->leftJoin('m.affectation', 'a');
            $query->andWhere('a.id = :id')
                  ->setParameter('id', $affectation);
        }

        if ($technique != null) {
            $query->leftJoin('m.technique', 't');
            $query->andWhere('t.id = :id')
                  ->setParameter('id', $technique);
        }

        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Materiel[] Returns an array of Materiel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Materiel
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
