<?php

namespace App\DataFixtures;

use Faker;
use Faker\Factory;
use App\Entity\Site;
use App\Entity\Materiel;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MaterielFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i = 1; $i <= 25; $i++) {
            $materiel = new Materiel();
            $materiel->setReference($faker->bothify('##-?????'));
            $materiel->setMarque(
                $faker->randomElement(
                    ['Sofranel', 'Krautkramer', 'General Electric', 'Olympus']
                )
            );
            $materiel->setType(
                $faker->randomElement(
                    ['TG101HR', 'MDP4-10', 'SLP5-5D', 'DP4-24', 'QC-5', 'MB4-S']
                )
            );
            $materiel->setDimension($faker->randomElement(['Ø5', 'Ø10', 'Ø24', '8x9']));
            $materiel->setFrequence($faker->randomElement([1, 2, 5, 10, 24]));

            $materiel->setCalibratedAt($faker->DateTimeBetween('-2 years', '+2 days'));
            $materiel->setEndCalibratedAt($faker->DateTimeBetween('-2 years', '+1 years'));
            $materiel->setIsAvalaible($faker->boolean());
            $materiel->setIsInVerification($faker->boolean());
            $materiel->setQuantity($faker->numberBetween(0, 100));

            $manager->persist($materiel);
        }

        $manager->flush();
    }
}
