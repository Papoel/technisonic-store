<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager)
    {
        // user non autorisé -- test
        /*
        $user = new User();
        $user->setFirstname('jean');
        $user->setLastname('reno');
        $user->setEmail("user@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_USER']);
        $user->setIsAutorised(0);

        $manager->persist($user);
        */
        
        // magasinier -- test
        /*
        $user = new User();
        $user->setFirstname('jean');
        $user->setLastname('dujardin');
        $user->setEmail("magasinier@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_MAGASINIER', 'ROLE_USER']);
        $user->setIsAutorised(1);

        $manager->persist($user);
        */
        
        // admin -- test
        /*
        $user = new User();
        $user->setFirstname('pascal');
        $user->setLastname('briffard');
        $user->setEmail("admin@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER', 'ROLE_MAGASINIER']);
        $user->setIsAutorised(1);

        $manager->persist($user);
        */
        
        // user autorisé
        // ! #################### 
        $user = new User();
        $user->setFirstname('nicolas');
        $user->setLastname('fourmaux');
        $user->setEmail("nicolas.fourmaux@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_USER']);
        $user->setIsAutorised(1);

        $manager->persist($user);

        // magasinier
        // ! #################### 
        $user = new User();
        $user->setFirstname('jonathan');
        $user->setLastname('fraiture');
        $user->setEmail("jonathan.fraiture@technisonic.fr");
        $user->setPassword($this->passwordHasher->hashPassword($user, 'password'));
        $user->setRoles(['ROLE_MAGASINIER', 'ROLE_USER']);
        $user->setIsAutorised(1);

        $manager->persist($user);

        $manager->flush();
    }
}
