<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MaterielRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MaterielRepository::class)
 * @ORM\Table(name="materiels", indexes={@ORM\Index(columns={"marque", "type"}, flags={"fulltext"})})
 * @Vich\Uploadable
 * @Gedmo\Loggable
 */
class Materiel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     * message = "Veuillez renseinger une référence."
     * )
     */
    private $reference;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     * message = "Veuillez renseinger la marque de matériel."
     * )
     */
    private $marque;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     * message = "Veuillez renseinger le type de matériel."
     * )
     */
    private $type;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dimension;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Positive
     */
    private $frequence;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $observation;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $calibratedAt;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\GreaterThan(propertyPath="calibratedAt",
     * message = "La date de fin de validité ne peut être inférieur à la date de vérification"
     * )
     */
    private $endCalibratedAt;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="integer")
     * @Assert\Positive
     *
     */
    private $quantity;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAvalaible;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInVerification;

    /**
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="materiels")
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity=Categorie::class, inversedBy="materiels")
     */
    private $categorie;

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="materiels")
     */
    private $affectation;

    // ***! VichUpload !*** \\
    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string|null
     */
    private $pvName;

    /**
     * NOTE : Il ne s'agit pas d'un champ mappé des métadonnées de l'entité, mais d'une simple propriété.
     * @Vich\UploadableField(mapping="certificat", fileNameProperty="pvName")
     */
    private $pvFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Technique::class, inversedBy="materiels")
     */
    private $technique;

    // ***! End VichUpload !*** \\

    public const CERTFICAT_VALIDATION_CONSTRAINTS = [
        'maxSize' => "8M",
        'mimeTypes' => ["application/pdf", "application/x-pdf"],
        'mimeTypesMessage' => "Veuillez charger un certificat au format pdf.",
    ];

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDimension(): ?string
    {
        return $this->dimension;
    }

    public function setDimension(string $dimension): self
    {
        $this->dimension = $dimension;

        return $this;
    }

    public function getFrequence(): ?string
    {
        return $this->frequence;
    }

    public function setFrequence(?string $frequence): self
    {
        $this->frequence = $frequence;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(?string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getCalibratedAt(): ?\DateTimeInterface
    {
        return $this->calibratedAt;
    }

    public function setCalibratedAt(?\DateTimeInterface $calibratedAt): self
    {
        $this->calibratedAt = $calibratedAt;

        return $this;
    }

    public function getEndCalibratedAt(): ?\DateTimeInterface
    {
        return $this->endCalibratedAt;
    }

    public function setEndCalibratedAt(?\DateTimeInterface $endCalibratedAt): self
    {
        $this->endCalibratedAt = $endCalibratedAt;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getIsAvalaible(): ?bool
    {
        return $this->isAvalaible;
    }

    public function setIsAvalaible(bool $isAvalaible): self
    {
        $this->isAvalaible = $isAvalaible;

        return $this;
    }

    public function getIsInVerification(): ?bool
    {
        return $this->isInVerification;
    }

    public function setIsInVerification(bool $isInVerification): self
    {
        $this->isInVerification = $isInVerification;

        return $this;
    }

    public function getUtilisateur(): ?User
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?User $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getAffectation(): ?Site
    {
        return $this->affectation;
    }

    public function setAffectation(?Site $affectation): self
    {
        $this->affectation = $affectation;

        return $this;
    }

    // ***! VichUpload Getter and Setter !*** \\

    public function getPvFile(): ?File
    {
        return $this->pvFile;
    }

    public function setPvFile(?File $pvFile = null): void
    {
        $this->pvFile = $pvFile;

        if (null !== $pvFile) {
            // Il est nécessaire qu'au moins un champ change si vous utilisez la doctrine
            // sinon les écouteurs d'événements ne seront pas appelés et le fichier sera perdu.
            $this->updatedAt = new \DateTime();
        }
    }

    public function getPvName(): ?string
    {
        return $this->pvName;
    }

    public function setPvName(?string $pvName): void
    {
        $this->pvName = $pvName;
    }

    // ***! End VichUpload Getter and Setter !*** \\


    /**
     * Get the value of updatedAt
     *
     * @return  \DateTimeInterface|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @param  \DateTimeInterface|null  $updatedAt
     *
     * @return  self
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTechnique(): ?Technique
    {
        return $this->technique;
    }

    public function setTechnique(?Technique $technique): self
    {
        $this->technique = $technique;

        return $this;
    }

}
