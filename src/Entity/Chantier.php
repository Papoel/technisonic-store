<?php

namespace App\Entity;

use App\Repository\ChantierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChantierRepository::class)
 */
class Chantier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Site::class, inversedBy="chantiers")
     */
    private $site;

    /**
     * @ORM\Column(type="integer")
     */
    private $tranche;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $codeArret;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $OTP;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="chantiers")
     */
    private $responsableMission;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getTranche(): ?int
    {
        return $this->tranche;
    }

    public function setTranche(int $tranche): self
    {
        $this->tranche = $tranche;

        return $this;
    }

    public function getCodeArret(): ?string
    {
        return $this->codeArret;
    }

    public function setCodeArret(?string $codeArret): self
    {
        $this->codeArret = $codeArret;

        return $this;
    }

    public function getOTP(): ?string
    {
        return $this->OTP;
    }

    public function setOTP(?string $OTP): self
    {
        $this->OTP = $OTP;

        return $this;
    }

    public function getResponsableMission(): ?User
    {
        return $this->responsableMission;
    }

    public function setResponsableMission(?User $responsableMission): self
    {
        $this->responsableMission = $responsableMission;

        return $this;
    }
}
