<?php

namespace App\Controller\EasyAdmin;

use Doctrine\Common\Collections\ArrayCollection;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminController extends EasyAdminController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function exportExcelAction()
    {
        $entity = $this->request->get('entity');

        if (! $entity) {
            throw new \Exception('Erreur');
        }

        $sortDirection = $this->request->query->get('sortDirection');
        if (empty($sortDirection) || !in_array(strtoupper($sortDirection), ['ASC', 'DESC'])) {
            $sortDirection = 'DESC';
        }

        $queryBuilder = $this->createListQueryBuilder(
            $this->entity['class'],
            $sortDirection,
            $this->request->query->get('sortField'),
            $this->entity['list']['dql_filter']
        );

        $entities = new ArrayCollection($queryBuilder->getQuery()->getArrayResult());

        if (! count($entities)) {
            throw new \Exception('Aucune données a importer');
        }

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        $row = 1;

        $headers = array_keys((array) $entities[0]);

        // headers
        foreach ($headers as $i => $header) {
            $sheet->setCellValueByColumnAndRow(++$i, $row, $this->translator->trans($this->humanize($header)));
        }

        // headers styling
        $styleArray = [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FFFFFF']
            ]
        ];

        $spreadsheet->getActiveSheet()
            ->getStyle(Coordinate::stringFromColumnIndex(1) .'1'. Coordinate::stringFromColumnIndex(count($headers)) .'1')
            ->applyFromArray($styleArray)
            ->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('066885')
        ;

        // write values
        foreach ($entities as $entry) {
            $row++; 
            $values = array_values($entry);

            foreach ($values as $i => $value) {
                $sheet->setCellValueByColumnAndRow(++$i, $row, $value);
            }
        }

        // autosize all columns
        foreach ($spreadsheet->getWorksheetIterator() as $worksheet) {
            $spreadsheet->setActiveSheetIndex($spreadsheet->getIndex($worksheet));

            $sheet = $spreadsheet->getActiveSheet();
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);

            foreach ($cellIterator as $cell) {
                $sheet->getColumnDimension($cell->getColumn())->setAutosize(true);
            }
        }

        $sheet->setTitle('Export - ' .$entity);

        // Create ans force download file
        $streamedResponse = new StreamedResponse();

        $streamedResponse->setCallback(function () use ($spreadsheet) {
            // $spreadsheet = // Création de mon spreadsheet ici
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
        });

        $streamedResponse->setStatusCode(200);
        $streamedResponse->headers->set('Content-Type', 'application/vnd.ms-excel');
        $streamedResponse->headers->set('Content-Disposition', 'attachment; filename="Technistore.xlsx"');

        return $streamedResponse->send();
    }

    private function humanize (string $value): string
    {
        return ucfirst(strtolower(trim(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', ' '], $value))));
    }
}