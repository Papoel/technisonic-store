<?php

namespace App\Controller\Materiel;

use App\Entity\Materiel;
use App\Form\UtFormType;
use App\Form\SearchMaterielType;
use App\Repository\MaterielRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MaterielController extends AbstractController
{
    private $repoMateriel;

    function __construct( MaterielRepository $repoMateriel )
    {
        $this->repoMateriel = $repoMateriel;
    }

    //! READ_ALL
    // Route principal affichant tous les produits pour le magasinier (action visible)
    #[Route('/materiel', name: 'app_materiel_index', methods:["GET"])]
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        //? Mise en place de la pagination
        $data = $this->repoMateriel->findAll();

        $materiels = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('materiels/index.html.twig', [
            'materiels' => $materiels,
        ]);
    }

    //! SEARCH
    #[Route('/materiel/recherche', name: 'app_materiel_search', methods:["GET"])]
    #[IsGranted('ROLE_MAGASINIER')]
    public function pageTest(Request $request, MaterielRepository $repoMateriel): Response
    {
        $materiels = $this->repoMateriel->findAll();
        // dd($materiels);

        // Executer une recherche par mot clé
        $form = $this->createForm(SearchMaterielType::class);
        $search = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $materiels = $repoMateriel->search(
                $search->get('mots')->getData(),
                $search->get('categorie')->getData(),
                $search->get('affectation')->getData(),
                $search->get('technique')->getData()
            );
        }

        return $this->render('materiels/search.html.twig', [
            'materiels' => $materiels,
            'form' => $form->createView(),
        ]);
    }

    //! READ_ONE
    // Route détaillant un produit
    #[Route('/materiel/{id<[0-9]+>}', methods: 'GET', name: 'app_materiel_show')]
    #[IsGranted('ROLE_USER')]
    public function show(Materiel $materiel): Response
    {
        return $this->render('materiels/show.html.twig', compact('materiel'));
    }

    //! CREATE
    // Ajout d'un nouveau matériel
    #[Route('/materiel/create/ultrason', methods:['GET', 'POST'], name:'app_materiel_create')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $materiel = new Materiel();

        $form = $this->createForm(UtFormType::class, $materiel);

        $form->handleRequest($request);

        if ($form -> isSubmitted() && $form->isValid()) {

            $em->persist($materiel);
            $em->flush();

            $this->addFlash('success', 'Matériel crée avec succès : ');

            return $this->redirectToRoute('app_materiel_show', ['id' => $materiel->getId()]);
        }

        return $this->renderForm('materiels/create.html.twig', [
            'form' => $form
        ]);
    }

    //! UPDATE
    // Modification ou mise à jour d'un produit
    #[Route('/materiel/{id<[0-9]+>}/edit', methods:['GET', 'POST'], name:'app_materiel_edit')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function edit(string $id, Materiel $materiel, MaterielRepository $repoMateriel, Request $request, EntityManagerInterface $em): Response
    {
        $productRepository = $em->getRepository(Materiel::class);
        $nameBeforePost = $repoMateriel->find($id)->getReference();

        $form = $this->createForm(UtFormType::class, $materiel);
        $form->handleRequest($request);

        // ? Vérifier si une référence est déjà existante avant de sauvegarder
        if($form->isSubmitted() && $form->isValid()) {

            if ($nameBeforePost !== $materiel->getReference() && $repoMateriel->findOneBy(['reference' => $materiel->getReference()])) {

                $this->addFlash('error', 'Référence déjà éxistante !');

                return $this->redirectToRoute('app_materiel_edit', ['id' => $request->get('id')]);

            }

            $em->flush();

            $this->addFlash('success', 'Modification sauvegardé');

            return $this->redirectToRoute('app_materiel_show', ['id' => $materiel->getId()]);
        }

        return $this->renderForm('materiels/edit.html.twig', [
            'materiel' => $materiel,
            'form' => $form
        ]);
    }

    //! DELETE
    // Effacer un matériel de la base de donnée
    #[Route('/materiel/{id<[0-9]+>}/delete', methods:'POST', name:'app_materiel_delete')]
    #[IsGranted('ROLE_MAGASINIER')]
    public function delete(Request $request, Materiel $materiel, EntityManagerInterface $em): Response
    {
        if ($this->isCsrfTokenValid('materiel_deletion_' . $materiel->getId(), $request->request->get('csrf_token')) ) {
            $em->remove($materiel);
            $em->flush();

            $this->addFlash('danger', 'Matériel '.$materiel->getMarque(). ' ' .$materiel->getType() .'a été supprimé !');
        }
            return $this->redirectToRoute('app_materiel_index');

    }


}
