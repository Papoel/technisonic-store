<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RedirectController extends AbstractController
{
    #[Route('/accces', methods: 'GET', name: "access")]
    public function index(UserInterface $user): Response
    {
        $userRoles = $user->getRoles();

        if (in_array("ROLE_ADMIN", $userRoles)) {
            return $this->redirectToRoute('admin');
        } elseif (in_array("ROLE_MAGASINIER", $userRoles)) {
            return $this->redirectToRoute('app_materiel_index');
        } else {
            return $this->redirectToRoute('app_materiel_index');
        }

    }
}
