<?php

namespace App\Controller;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AwsController extends AbstractController
{
    #[Route('/aws', name: 'aws')]
    public function index(): Response
    {

        $client = new S3Client([
        'endpoints' => '%env(string:AWS_ENDPOINT)%',
        'credentials' => [
            'key' => '%env(string:AWS_IAM_ACCES_KEY)%',
            'secret' => '%env(string:AWS_IAM_SECRET_KEY)%'
        ],
        'region' => 'eu-west-3',
        'version' => 'latest',
    ]);

    $adapter = new AwsS3V3Adapter($client, 'technistore');

    // dd($adapter);

    $filesystem = new Filesystem($adapter);

    $files = $filesystem->listContents('/');

    // dd($files);

    return $this->render('aws/check.html.twig', [
        'files' => $files,
    ]);


    }
}
