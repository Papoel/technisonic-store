<?php

namespace App\Controller\Admin;

use App\Entity\Site;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SiteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Site::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Site')
                ->addCssClass('text-capitalize'),
            TextField::new('adress', 'Adresse')
                ->setColumns('col-6'),
            NumberField::new('codePostal', 'Code paostal')
                ->setColumns('col-6'),
            TextField::new('town', 'Ville')
                ->setColumns('col-6'),
            NumberField::new('departement', 'Département')
                ->setColumns('col-6'),
            TelephoneField::new('telephone', 'N° Téléphone'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions->add(Crud::PAGE_INDEX, 'detail');
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Liste des CNPE et Agence')
            ->setPageTitle('new', 'Ajouter un site')
            ->setPageTitle('edit', fn (Site $site) => sprintf('Modification du site : <i class="text-danger">%s</i>',
                $site->getName()))
            ->setPageTitle('detail', fn (Site $site) =>
                $site->getName()
                .' ('.
                $site->getDepartement()
                .')'
            );

    }
}
