<?php

namespace App\Controller\Admin;

use App\Entity\Technique;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TechniqueCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Technique::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Technique')
                ->addCssClass('text-capitalize'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Liste des techniques')
            ->setPageTitle('new', 'Ajouter une nouelle technique');
    }
}
