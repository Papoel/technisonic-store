<?php

namespace App\Controller\Admin;

use App\Entity\Site;
use App\Entity\User;
use App\Entity\Chantier;
use App\Entity\Materiel;
use App\Entity\Categorie;
use App\Entity\Technique;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class AdminDashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('EasyAdminBundle/dashboard.html.twig');
        // return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('
                <span>Technistore</span>
                <span class="text-success"> - Sytème de gestion du matériel</span>
            ', 'far fa-chart-bar');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Application', 'fas fa-grimace', 'app_materiel_index');
        yield MenuItem::linkToCrud('Le Matériel', 'fas fa-warehouse', Materiel::class);
        yield MenuItem::linkToCrud('Les Catégories', 'fas fa-tags', Categorie::class);
        yield MenuItem::linkToCrud('Les Chantiers', 'fas fa-radiation', Chantier::class);
        yield MenuItem::linkToCrud('Les Sites', 'fas fa-globe', Site::class);
        yield MenuItem::linkToCrud('Les Techniques', 'fas fa-dice-d20', Technique::class);
        yield MenuItem::section('Personnels');
        yield MenuItem::linkToCrud('Les Employés', 'fas fa-id-card', User::class);
        yield MenuItem::linkToLogout('Se déconnecter', 'fas fa-power-off');
    }

}
