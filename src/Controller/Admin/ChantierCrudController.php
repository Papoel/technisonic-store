<?php

namespace App\Controller\Admin;

use App\Entity\Chantier;
use App\Form\ChantierFormType;
use Doctrine\DBAL\Types\TextType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;

class ChantierCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Chantier::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $fields = [
            AssociationField::new('site', 'Site :')
                ->setColumns('col-4')
                ->addCssClass('text-capitalize'),

            IntegerField::new('tranche', 'Tranche :')
                ->setColumns('col-4'),

            TextField::new('codeArret')
                ->setColumns('col-4'),

            TextField::new('otp', 'OTP : ')
                ->setColumns('col-6'),
        ];

        return $fields;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('new', 'Créer un chantier');
    }

}
