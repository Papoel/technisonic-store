<?php

namespace App\Controller\Admin;

use App\Entity\Materiel;
use Vich\UploaderBundle\Form\Type\VichFileType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use Symfony\Component\Validator\Constraints\File;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class MaterielCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Materiel::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $certificatFile = TextField::new(
            'pvFile',
            'Insérer le certificat de conformité')
            ->setFormType(VichFileType::class)
            ->setFormTypeOptions([
                'attr' => ['accept' => 'application/pdf'],
                'constraints' => [
                    new File(Materiel::CERTFICAT_VALIDATION_CONSTRAINTS),
                ]
            ]);

        $certificatName = TextareaField::new('pvName', 'Certificat');

        $fields =  [
            TextField::new('marque')
                ->setColumns('col-sm-4 col-lg-2'),

            TextField::new('type')
                ->setColumns('col-sm-4 col-lg-2'),

            TextField::new('reference', 'Référence')
                ->setColumns('col-sm-4 col-lg-2'),

            IntegerField::new('quantity', 'Quantité')
                ->setColumns('col-sm-4 col-lg-2')
                ->setFormTypeOption('constraints', [
                    new PositiveOrZero(),
                ]),

            TextField::new('dimension')
                ->hideOnIndex()
                ->setColumns('col-sm-4 col-lg-2'),

            IntegerField::new('frequence')
                ->setColumns('col-sm-4 col-lg-2')
                ->hideOnIndex(),

            AssociationField::new('utilisateur', 'Dotation')
                ->addCssClass('text-capitalize')
                ->setColumns('col-6')
                ->autoComplete(),

            AssociationField::new('technique', 'Technique')
                ->setColumns('col-6')
                ->addCssClass('text-capitalize'),

            AssociationField::new('categorie', 'Catégorie')
                ->hideOnIndex()
                ->addCssClass('text-capitalize')
                ->setColumns('col-6'),

            AssociationField::new('affectation', 'Affectation')
                ->addCssClass('text-capitalize')
                ->setColumns('col-6')
                ->hideOnIndex(),

            TextEditorField::new('observation', 'Ajouter une note :')
                ->setColumns('col-12')
                ->hideOnIndex(),

            DateField::new('calibrated_at', 'Début de validité')
                ->setColumns('col-6 col-md-4 col-lg-4'),

            DateField::new('end_calibrated_at', 'Fin de validité')
                ->setColumns('col-6 col-md-4 col-lg-4'),

            BooleanField::new('isAvalaible', 'Disponible')
                ->setColumns('col-6'),

            BooleanField::new('isInVerification', 'Indisponible')
                ->setColumns('col-6'),

        ];

        if ($pageName == Crud::PAGE_INDEX || $pageName == Crud::PAGE_DETAIL) {
            $fields[] = $certificatName;
        } else {
            $fields[] = $certificatFile;
        }

        return $fields;
    }

    public function analyseDate()
    {

    }


    public function configureActions(Actions $actions): Actions
    {
        return $actions->add(Crud::PAGE_INDEX, 'detail');
        // Ajouter un bouton pour exporter en csv les resultats ?? ...
        return $actions->add(Crud::PAGE_DETAIL, 'export');
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['endCalibratedAt' => 'ASC'])
            ->setPageTitle('index', 'Liste du matériel')
            ->setPageTitle(
                'detail',
                fn (Materiel $materiel) =>
                $materiel->getCategorie()
                    . ' ' .
                    $materiel->getReference()
            )
            ->setPageTitle(
                'edit',
                fn (Materiel $materiel) => sprintf(
                    "Modification <span class='fw-bold text-danger'>%s</span>",
                    $materiel->getReference()
                )
            )
            ->setPageTitle('new', fn () => 'Ajouter un nouveau matériel');
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            // ->add(EntityFilter::new('marque'))
            ->add('marque')
            ->add('reference')
            ->add('categorie')
            ->add('type')
            ->add('technique')
            ->add('utilisateur')
            ->add('isAvalaible', ['content'=>'Disponible'])
            ->add('affectation');
    }
}
