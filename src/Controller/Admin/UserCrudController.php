<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),

            TextField::new('fullName', 'Nom Complet')->hideOnForm(),

            TextField::new('firstname', "Prénom")->setColumns('col-sm-6 col-lg-5 col-xxl-3')->addCssClass('text-danger'),

            TextField::new('lastname', "Nom")->setColumns('col-sm-6 col-lg-5 col-xxl-3'),

            ChoiceField::new ('roles')->setChoices([
                'ROLE_ADMIN' => 'ROLE_ADMIN',
                'ROLE_MAGASINIER' => 'ROLE_MAGASINIER',
                'ROLE_TECHNICIEN' => 'ROLE_USER',
            ])->allowMultipleChoices(),

            EmailField::new('email', "Email",[
                'input' => 'fullname'
            ]),

            TextField::new('password', 'Mot de passe')->onlyWhenCreating(),

            BooleanField::new('isAutorised', "Autoriser l'accès")
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['lastname' => 'ASC'])
            ->setPageTitle('index', 'Liste du personnels')
            ->setPageTitle('new', fn () => 'Ajouter un nouvel employé');

    }
}
